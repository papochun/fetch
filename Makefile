CC = gcc
CFLAGS = -std=c99 -pedantic -Wall -Wno-deprecated-declarations -Os
TARGET = fetch
SRC = main.c
PREFIX = /usr/local/bin

$(TARGET): $(SRC)
	$(CC) -o $(TARGET) $(SRC) $(CFLAGS)

clean:
	rm -f $(TARGET)

install: $(TARGET)
	mkdir -p $(PREFIX)
	cp -f $(TARGET) $(PREFIX)
	chmod 755 $(PREFIX)/$(TARGET)

uninstall:
	rm -f $(PREFIX)/$(TARGET)
