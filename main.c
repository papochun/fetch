#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/statvfs.h>

#include "color.h"

struct string {
	char string[1024];
};

struct string flw(char *file, int line, int word) {
	struct string buffer, result;

	if (access(file, F_OK) != 0) {
		result.string[0]='\0';
		return result;
	}

	FILE *fp = fopen(file, "r");
	for (int i = 1; i <= line; i++)
		if (fgets(buffer.string, sizeof(buffer.string), fp) == NULL)
			exit(1);
	fclose(fp);

	strncpy(result.string, strtok(buffer.string, " "), sizeof(result.string));
	for (int i = 1; i != word; i++) {
		strncpy(result.string, strtok(NULL, " "), sizeof(result.string));
		if (result.string[0] == '\0' )
			exit(1);
	}

	if (result.string[strlen(result.string)-1] == '\n')
		result.string[strlen(result.string)-1] = '\0';

	return result;
}

static struct string match(char *file, char *word) {
	struct string line;

	FILE *fp = fopen(file, "r") ;
	while (fgets(line.string, sizeof(line.string), fp) != NULL)
		if (strstr(line.string, word) != NULL)
			break;
	fclose(fp);

	return line;
}

static int memory() {
	int total = 	atoi(flw("/proc/meminfo", 1, 2).string);
	int free = 	atoi(flw("/proc/meminfo", 2, 2).string);
	int buffers = 	atoi(flw("/proc/meminfo", 4, 2).string);
	int cached = 	atoi(flw("/proc/meminfo", 5, 2).string);
	int slab = 	atoi(flw("/proc/meminfo", 24, 2).string);

	return (total - free - buffers - cached - slab) / 1000;
}

static struct string uptime() {
	struct string uptime = flw("/proc/uptime", 1, 1);

	double hour = atof(uptime.string) / 60 / 60;
	double min = (hour - (int)hour) * 60;

	sprintf(uptime.string, "%dH %dM", (int)hour, (int)min);

	return uptime;
}

static struct string os() {
	struct string processed, os = match("/etc/os-release", "NAME=");

	os.string[strlen(os.string) - 2] = '\0';
	strncpy(processed.string, os.string + 6, sizeof(processed.string));

	return processed;
}

static struct string load() {
	return flw("/proc/loadavg", 1, 1);
}

static struct string host() {
	return flw("/proc/sys/kernel/hostname", 1, 1);
}

static struct string kernel() {
	return flw("/proc/version", 1, 3);
}

static struct string disk() {
	struct statvfs buffer;
	struct string result;

	statvfs("/", &buffer);

	const double GB = (1024 * 1024) * 1024;
	const double total = (buffer.f_blocks * buffer.f_frsize) / GB;
	const double available = (buffer.f_bfree * buffer.f_frsize) / GB;
	const double used = total - available;

	sprintf(result.string, "%0.fGb/%0.fGb", used, total);
	return result;
}

int main() {
	fprintf(stdout, "%s@%s\n", getenv("USER"), host().string);

	fprintf(stdout, "OS       %s\n", os().string);
	fprintf(stdout, "LOAD     %s\n", load().string);
	fprintf(stdout, "KERNEL   %s\n", kernel().string);
	fprintf(stdout, "UPTIME   %s\n", uptime().string);
	fprintf(stdout, "MEMORY   %dMb\n", memory());
	fprintf(stdout, "DISK     %s\n", disk().string);
	fprintf(stdout, "SHELL    %s\n", getenv("SHELL"));

	fprintf(stdout, "\n");
	fprintf(stdout, "%s  %s  %s  %s  %s  %s  %s  %s  \n", b0, b1, b2, b3, b4, b5, b6, b7);
	fprintf(stdout, "%s  %s  %s  %s  %s  %s  %s  %s  %s\n", b8, b9, b10, b11, b12, b13, b14, b15, c);
	fprintf(stdout, "\n");
}
